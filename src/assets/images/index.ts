import ppl1 from './ppl1.svg'
import ppl2 from './ppl2.svg'
import ppl3 from './ppl3.svg'
import ppl4 from './ppl4.svg'
import ppl5 from './ppl5.svg'
import ppr1 from './ppr1.svg'
import ppr2 from './ppr2.svg'
import ppr3 from './ppr3.svg'
import ppr4 from './ppr4.svg'
import ppr5 from './ppr5.svg'
import ppr6 from './ppr6.svg'
import pplbg from './pplbg.png'
import pprbg from './pprbg.png'

interface Images {
    [key: string]: string
}

const images: Images = {ppl1, ppl2, ppl3, ppl4, ppl5, ppr1, ppr2, ppr3, ppr4, ppr5, ppr6, pplbg, pprbg}

export default images
