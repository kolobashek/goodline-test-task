import React, { useState } from 'react'
import './Drawer.scss'
import { Link } from 'react-router-dom'
import { MdMenu } from "react-icons/md"

const Drawer = () => {
    const [isClosed, setClose] = useState(true)
    const wrapperClass = `opacityWrapper ${isClosed ? 'hide' : 'show'}`
    const drawerClass = `drawer ${isClosed ? 'closed' : 'opened'}`
    return (
        <div>
            <div className='menubutton' onClick={() => setClose(!isClosed)}>
                <MdMenu className='menuicon' />
            </div>
            <div className={drawerClass}>
                <button className='closebtn' onClick={() => setClose(true)}>&times;</button>
                <Link to='/' onClick={() => setClose(true)}>Table</Link>
                <Link to='/frontend_design' onClick={() => setClose(true)}>Frontend design</Link>
            </div>
            <div className={wrapperClass} onClick={() => setClose(true)} />
        </div>
    )
}

export default Drawer