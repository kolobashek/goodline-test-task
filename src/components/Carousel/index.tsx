import React, { useState } from 'react'
import './style.scss'
import { MdKeyboardArrowDown } from 'react-icons/md'

interface ReviewData {
    name: string,
    photo: string,
    review: string
}

export default function Carousel({ data }: any) {
    const [current, setCurrent] = useState(0)
    function nextSlide(currentSlide: number) {
        const slideQuantity = data.length
        if (currentSlide !== (slideQuantity - 1)) {
            setCurrent(currentSlide + 1)
        }else setCurrent(0)
    }
    function inactive(key: number) {
        if ( current !== key ) return 'inactive'
        else return 'active'
    }
    function SliderItem(data: ReviewData, key: number) {
        return (
            <div className={`slider-item ${inactive(key)}`} key={key}>
                <div className='avatar-border'>
                    <img src={data.photo} alt='' />
                </div>
                <div className='slider-item-text-container'>
                    <span className='slider-item-name'>{data.name}</span><br />
                    <span>{data.review}{key}</span>
                </div>
                <MdKeyboardArrowDown className='carousel-button' onClick={() => nextSlide(key)} />
            </div>
        )
    }
    // console.log(data)
    return (
        <div className='slider'>
            {data.map(SliderItem)}
        </div>
    )
}