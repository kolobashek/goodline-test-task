import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

test('renders table', () => {
  render(<App />);
  const linkElement = screen.getByText(/Singleton/i);
  expect(linkElement).toBeInTheDocument();
});
