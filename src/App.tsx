import React from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom'
import Drawer from './components/Drawer'
import FrontendDesign from './views/FrontendDesign';
import Table from './views/Table'

export default function App() {
  return (
    <Router>
      <div>
        <nav>
          <Drawer />
        </nav>
        <Switch>
          <Route path='/frontend_design'>
            <FrontendDesign />
          </Route>
          <Route path='/'>
            <Table />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}
