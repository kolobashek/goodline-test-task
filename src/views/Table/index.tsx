import React from 'react'
import data from '../../assets/data.json'
import './table.css'

interface DataItem {
    name: string,
    id: string,
    phone: string,
    age: number
}

function TableRow(item: DataItem, key: number) {
    const {name, id, phone, age} = item
    return (
        <tr key={key}>
            <td>{name}</td>
            <td>{id}</td>
            <td>{phone}</td>
            <td>{age}</td>
        </tr>
    )
}
export default function Table() {
    return (
        <div className='container'>
            <table className='table'>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Id</th>
                        <th>Phone</th>
                        <th>Age</th>
                    </tr>
                </thead>
                <tbody>
                    {data.map(TableRow)}
                </tbody>
            </table>
        </div>
    )
}