import React, { useState } from 'react'
import cam from '../../assets/images/cam.png'
import erPic from '../../assets/images/error.png'
import icLogoText from '../../assets/images/ic-logo-text.svg'
import icLogo from '../../assets/images/ic-logo2.svg'
import icsend from '../../assets/images/ic-send.svg'
import item1 from '../../assets/images/item1.svg'
import item2 from '../../assets/images/item2.svg'
import item3 from '../../assets/images/item3.svg'
import item4 from '../../assets/images/item4.svg'
import popupData from '../../assets/popupdata.json'
import data from '../../assets/reviewsData.json'
import Carousel from '../../components/Carousel'
import './style.scss'
import images from '../../assets/images'
// import * as images from '../../assets/images'

interface PopupItem {
    img: string,
    text: string
}

// const images = {ppl1, ppl2, ppl3, ppl4, ppl5, ppr1, ppr2, ppr3, ppr4, ppr5, ppr6}

export default function FrontendDesign() {
    const [phone, setPhone] = useState('')
    const [inputStatus, setInputStatus] = useState('normal')
    const [popupStatus, setPopupStatus] = useState(0)
    const [submitSuccess, setSubmitSuccess] = useState(false)

    function normalizePhoneNumber(value: string) {
        if (!value) return value
        const currentValue = value.replace(/[^\d]/g, '')
        const cvLength = currentValue.length
        setPhone(previousValue => {
            const phoneNumber = `+7 ${currentValue.slice(1, 4)} ${currentValue.slice(4, 7)} ${currentValue.slice(7, 11)}`
            if (currentValue === previousValue) setInputStatus('filled')
            // if (value.length < previousValue.length) return currentValue
            if (value.length < previousValue.length) {
                setInputStatus('input')
                return phoneNumber.trimEnd()
            }
            if (!previousValue || value.length > previousValue.length) {
                if (cvLength === 1) {
                    if (value === '8') {
                        return `+7 `
                    } else return `+7 ${currentValue}`
                }
                if (cvLength === 11) phoneValidate(phoneNumber)
                return phoneNumber
            } else return previousValue
        })
    }

    function phoneValidate(number: string) {
        if (!number.match(/^[\+][0-9][\s][0-9]{3}[\s][0-9]{3}[\s][0-9]{4}$/im) || number === '+7 999 999 9999') { //second "BUG" condition for example
            setInputStatus('error')
        } else setInputStatus('filled')
    }

    async function submitCall() {
        if (inputStatus === 'filled') {
            setSubmitSuccess(true)
            const response = await fetch('https://myjson.com', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    phone: phone,
                })
            })
            // if (!response.ok) {
            //     const message = `An error has occured: ${response.status}`;
            //     throw new Error(message);
            // }
            // const callback = await response.json()
            // return callback
            return          //for example
        } else phoneValidate(phone)
    }

    const PopupGridItem = (data: PopupItem, key: number) => {
        const getKeyValue = <T extends object, U extends keyof T>(key: U) => (obj: T) => obj[key]
        return (
            <div className="popup-grid-item" key={key}>
                <div>
                    <div style={{ backgroundImage: `url(${images[data.img]}), linear-gradient(180deg, #8BB928 0%, #45AE4D 100%)` }} />
                    <span>{data.text}</span>
                </div>
            </div>
        )
    }

    return (
        <div className='fe-container'>
            <div className='block1'>
                <div className='logo-wrapper'>
                    <img className='logo' src={icLogo} alt='' />
                    <img className='logo-text' src={icLogoText} alt='' />
                </div>
                <span className='block1-text'>
                    Комплекс из камеры и мобильного приложения, который помогает в любой момент убедиться, что с близкими и домом всё в порядке.
                </span>
            </div>
            <div className='block2'>
                <div className='block2-item' onClick={() => setPopupStatus(1)}>
                    <div className='shadow'>
                        <div className='cam-bg'>
                            <div style={{ backgroundImage: `url(${cam})` }} />
                            {/* <img src={cam} alt='' /> */}
                        </div>
                    </div>
                    <span>ПОДРОБНЕЕ</span>
                </div>
                <div className='block2-item' onClick={() => setPopupStatus(2)}>
                    <div className='shadow'>
                        <div className='cam-bg'>
                            <div style={{ backgroundImage: `url(${cam})` }} />
                            {/* <img src={cam} alt='' /> */}
                        </div>
                    </div>
                    <span>ПОДРОБНЕЕ</span>
                </div>
                <div className={`popup ${popupStatus === 1 ? 'show' : 'hide'}`} onClick={() => setPopupStatus(0)}>
                    <div className='popup-container popup-bg-l' style={{ backgroundImage: `url(${images['pplbg']})` }}>
                        <button className="popup-close-button" onClick={() => setPopupStatus(0)}>&times;</button>
                        <div className="popup-content-wrapper" onClick={e => e.stopPropagation()}>
                            {popupData.map((item, key) => {
                                if (item.img.charAt(2) === 'l') return PopupGridItem(item, key)
                                return null
                            })}
                        </div>
                    </div>
                </div>
                <div className={`popup ${popupStatus === 2 ? 'show' : 'hide'}`} onClick={() => setPopupStatus(0)}>
                    <div className='popup-container popup-bg-r' style={{ backgroundImage: `url(${images['pprbg']})` }}>
                        <div className="popup-content-wrapper" onClick={e => e.stopPropagation()}>
                            {popupData.map((item, key) => {
                                if (item.img.charAt(2) === 'r') return PopupGridItem(item, key)
                                return null
                            })}
                        </div>
                        <button className="popup-close-button" onClick={() => setPopupStatus(0)}>&times;</button>
                    </div>
                </div>
            </div>
            <div className='block3'>
                <h1>Приобрести</h1>
                <div className='block3-items-wrapper'>
                    <div>
                        <img src={item1} alt='' />
                        <span>Покупка</span>
                    </div>
                    <div>
                        <img src={item2} alt='' />
                        <span>Покупка</span>
                    </div>
                    <div>
                        <img src={item3} alt='' />
                        <div>
                            <span>Пакет</span><br />
                            <span>“Все под контролем”</span>
                        </div>
                    </div>
                    <div>
                        <img src={item4} alt='' />
                        <div>
                            <span>Пакет</span><br />
                            <span>“Тысяча пятьсот”</span>
                        </div>
                    </div>
                </div>
            </div>
            <div className='block4'>
                {
                    !submitSuccess ?
                        <div className={`phone ${inputStatus}`}><span>Оставьте заявку, и мы позвоним вам в ближайшее время</span>
                            <input
                                type='tel'
                                name='phone'
                                value={phone}
                                placeholder='+7 ___ ___ ____'
                                onChange={e => normalizePhoneNumber(e.currentTarget.value)}
                                inputMode='numeric'
                                onFocus={() => setInputStatus('input')}
                                onBlur={() => phoneValidate(phone)}
                                autoComplete='off'
                            ></input>
                            {!(inputStatus === 'error') ?
                                <label htmlFor='phone'>Контактный телефон</label>
                                : <label htmlFor='phone'><img src={erPic} alt='' />Проверьте набранный номер</label>}
                        </div>
                        : <div className={`phone ${inputStatus}`}><span className='success'>Спасибо, что оставили заявку.<br />Мы скоро вам перезвоним.</span></div>
                }
                <button disabled={submitSuccess} onClick={submitCall}>
                    <span>отправить заявку</span>
                    <img src={icsend} alt='' />
                </button>
            </div>
            <div className='block5'>
                <h1>Отзывы</h1>
                <Carousel data={data} />
            </div>
        </div>
    )
}